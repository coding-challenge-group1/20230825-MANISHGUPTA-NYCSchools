//
//  DetailsViewModel.swift
//  Schools
//
//  Created by Manish Gupta on 8/25/23.
//

import Foundation

class DetailsViewModel: ObservableObject {
    var school: School
    @Published var satScore: SATScore = SATScore()
    
    var networkRequest = NetworkRequest.sharedInstance
    
    init(school: School) {
        self.school = school
        fetchSatScore(dbn: school.dbn ?? "01M292")
    }
    
    func fetchSatScore(dbn: String) {
        if let theURL = URL(string: SchoolAPI.SAT.score + "dbn=\(dbn)") {
            networkRequest.callAPI(with: theURL, forType: SATScore.self) { scores in
                self.satScore = scores?.first ?? SATScore()
            }
        }
    }
}


