//
//  ListView.swift
//  Schools
//
//  Created by Manish Gupta on 8/24/23.
//

import SwiftUI

struct ListView: View {
    @ObservedObject private var vm = ListViewModel()
    
    var body: some View {
        NavigationView {
            ScrollView {
                VStack(spacing: 10) {
                    ForEach(vm.isSearching ? vm.filteredSchools : vm.schools, id: \.self) { school in
                        NavigationLink {
                            DetailsView(viewModel: DetailsViewModel(school: school))
                        } label: {
                            VStack(alignment: .leading, spacing: 10) {
                                HStack(alignment: .top) {
                                    Text(school.school_name ?? "")
                                        .font(.headline)
                                    Spacer()
                                    Text(school.borough ?? "")
                                        .font(.caption)
                                }
                                Text(school.admissionspriority11 ?? "Priority given to first come basis")
                                    .font(.caption)
                            }
                            .padding()
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .background(Color.black.opacity(0.05))
                            
                        }.buttonStyle(.plain)
                    }
                }.padding()
            }
            .navigationTitle("Schools in NYC")
            .navigationBarTitleDisplayMode(.inline)
            .searchable(text: $vm.searchText, placement: .automatic, prompt: "Search with School Names, Cities, Boroughs")
        }
    }
}

struct SchoolsListView_Previews: PreviewProvider {
    static var previews: some View {
        ListView()
    }
}
