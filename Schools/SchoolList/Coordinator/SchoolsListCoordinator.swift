//
//  SchoolsListCoordinator.swift
//  Schools
//
//  Created by Manish Gupta on 8/24/23.
//

import Foundation
import SwiftUI

class SchoolsListCoordinator: CoordinatorProtocol {
    var navController: UINavigationController
    
    init(navController: UINavigationController) {
        self.navController = navController
    }
    
    func start() {
        let schoolsListView = UIHostingController(rootView: ListView())
        navController.pushViewController(schoolsListView, animated: true)
    }
}
