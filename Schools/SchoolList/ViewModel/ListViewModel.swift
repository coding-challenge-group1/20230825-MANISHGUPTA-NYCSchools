//
//  ListViewModel.swift
//  Schools
//
//  Created by Manish Gupta on 8/24/23.
//

import Foundation
import Combine

class ListViewModel: ObservableObject {
    @Published private(set) var schools: [School] = []
    @Published private(set) var filteredSchools: [School] = []
    @Published var searchText: String = ""
    
    var cancellables = Set<AnyCancellable>()
    var isSearching: Bool {
        !searchText.isEmpty
    }
    
    let networkRequest = NetworkRequest.sharedInstance
    
    init() {
        fetchSchools()
        addSubscribers()
    }
    
    private func addSubscribers() {
        $searchText
            .debounce(for: 0.3, scheduler: DispatchQueue.main)
            .sink { [weak self] searchText in
                self?.filterSchools(searchText: searchText)
            }.store(in: &cancellables)
    }
    
    private func filterSchools(searchText: String) {
        guard !searchText.isEmpty else {
            filteredSchools = []
            return
        }
        
        let search = searchText.lowercased()
        filteredSchools = schools.filter({ school in
            let title = school.school_name?.lowercased().contains(search) ?? false
            let city = school.city?.lowercased().contains(search) ?? false
            let borough = school.borough?.lowercased().contains(search) ?? false
            
            return (title) || (city) || (borough)
        })
    }
    
    func fetchSchools() {
        if let theURL = URL(string: SchoolAPI.List.schoolList) {
            networkRequest.callAPI(with: theURL, forType: School.self) { schools in
                if let schools = schools {
                    self.schools = schools
                }
            }
        }
    }
    
}
