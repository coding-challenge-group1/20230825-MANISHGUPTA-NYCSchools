//
//  CoordinatorProtocol.swift
//  Schools
//
//  Created by Manish Gupta on 8/24/23.
//

import Foundation
import UIKit

protocol CoordinatorProtocol {
    var navController: UINavigationController { get set }
    func start()
}
