//
//  Service.swift
//  Schools
//
//  Created by Manish Gupta on 8/25/23.
//

import Foundation
import Combine

enum SchoolAPI {
    struct List {
        static let schoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
    }
    
    struct SAT {
        static var score = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?"
    }
}

class NetworkRequest {
    var error: Error?
    private var cancellable = Set<AnyCancellable>()
    private var detailsSubscriber = Set<AnyCancellable>()
    
    private init() {}
    static let sharedInstance = NetworkRequest()
    
    /// This provides the list of the objects
    func callAPI<T: Decodable>(with url: URL,
                               forType: T.Type,
                               list: @escaping(([T]?) -> Void)) {
        let _ = URLSession.shared.dataTaskPublisher(for: url)
            .map { (data, reponse) in
                return data
            }
            .decode(type: [T].self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: break
                case .failure(let error):
                    self?.error = error
                }
            } receiveValue: { schools in
                DispatchQueue.main.async {
                    list(schools)
                }
            }
            .store(in: &cancellable)
    }
    
    /// This provides the details of a selected object
    func callAPIForDetails<T: Decodable>(with url: URL,
                               forType: T.Type,
                               details: @escaping((T?) -> Void)) {
        let _ = URLSession.shared.dataTaskPublisher(for: url)
            .map { (data, reponse) in
                return data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .sink { [weak self] completion in
                switch completion {
                case .finished: break
                case .failure(let error):
                    self?.error = error
                }
            } receiveValue: { score in
                DispatchQueue.main.async {
                    details(score)
                    print(score)
                }
            }
            .store(in: &detailsSubscriber)
    }
    
}

